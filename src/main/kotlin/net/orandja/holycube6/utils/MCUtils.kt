package net.orandja.holycube6.utils

import net.minecraft.network.MessageType
import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.text.LiteralText
import net.minecraft.text.Text
import net.minecraft.util.Util

fun ServerPlayerEntity.sendHUD(message: Any) {
    sendMessage(message as? Text ?: LiteralText(message.toString()), MessageType.GAME_INFO, Util.NIL_UUID)
}